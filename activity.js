db.fruits.aggregate([
	{$match: {$and:[{supplier: "Yellow Farms"},{price:{$lt:50}}]}},
	{$count: "itemsYellowFarms"}
])

db.fruits.aggregate([
	{$match: {price:{$lt:30}}},
	{$count: "itemLT50"}
])

db.fruits.aggregate([
	{$match: {supplier: "Yellow Farms"}},
	{$group:{_id:"avgPriceYellowFarms", avgPrice: {$avg: "$price"}}}
])

db.fruits.aggregate([
	{$match: {supplier: "Red Farms Inc."}},
	{$group:{_id:"highestPriceRedFarms", maxPrice: {$max: "$price"}}}
])

db.fruits.aggregate([
	{$match: {supplier: "Red Farms Inc."}},
	{$group:{_id:"lowestPriceRedFarms", minPrice: {$min: "$price"}}}
])
