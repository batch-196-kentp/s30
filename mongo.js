db.fruits.insertMany(
	[
		{
			"name": "Apple",
			"supplier": "Red Farms inc.",
			"stocks": 20,
			"price": 40,
			"onSale": true
		},
		{
			"name": "Banana",
			"supplier": "Yellow Farms",
			"stocks": 15,
			"price": 20,
			"onSale": true
		},
		{
			"name": "Kiwi",
			"supplier": "Green Farming and Canning",
			"stocks": 25,
			"price": 50,
			"onSale": true
		},
		{
			"name": "Mango",
			"supplier": "Yellow Farms",
			"stocks": 10,
			"price": 60,
			"onSale": true
		},
		{
			"name": "Dragon Fruit",
			"supplier": "Red Farms Inc.",
			"stocks": 10,
			"price": 60,
			"onSale": true
		}
	]
)

/*
	Aggregation Pipeline Stages
*/
//$avg - is an operator used in $group stage
//$avg gets the avg of the numerical values of the indicated field in grouped documents

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock: {$avg:"$stocks"}}}

])

//$max - allow us to get the highest value out of the values in a given field per grooup

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id: "highestStockOnSale", maxStock: {$max: "$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id: null, maxPrice: {$max: "$price"}}}

])

//$min - allow us to get the lowest value out of the values in a given field per group

//get the lowest number of stock for all items in sale
db.fruits.aggregate([
	
	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock: {$min: "$stocks"}}}

])

db.fruits.aggregate([
	
	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}

])

db.fruits.aggregate([
	
	{$match: {price: {$lt: 50}}},
	{$group: {_id:"lowestStock", minStock: {$min: "$stocks"}}}

])

